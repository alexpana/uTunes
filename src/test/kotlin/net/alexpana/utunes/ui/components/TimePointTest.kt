package net.alexpana.utunes.ui.components

import org.junit.Assert.assertEquals
import org.junit.Test

class TimePointTest {

    @Test
    fun testToString() {
        val timePoint = TimePoint(0, 0, 1)
        assertEquals("00:00:01.000", timePoint.toString())
    }

    @Test
    fun testFromSeconds() {
        assertEquals(TimePoint(1, 2, 34), TimePoint.fromSeconds(1 * 60 * 60 + 2 * 60 + 34))
    }
}