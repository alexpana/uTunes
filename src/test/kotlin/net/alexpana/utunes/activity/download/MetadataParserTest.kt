package net.alexpana.utunes.activity.download

import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class MetadataParserTest {

    @Test
    fun shouldParsePlaylists() {
        val output = String(javaClass.classLoader.getResourceAsStream("playlist.json").readBytes(1024 * 1024))

        val provider = MetadataParser()

        val item = provider.parse(output)

        Assert.assertThat(item, Is.`is`(DownloadItem("Alcest - Shelter", "rqYCbyjvmQQ")))
    }

    @Test
    fun shouldParseSingleItem() {
        val output = String(javaClass.classLoader.getResourceAsStream("single_item.json").readBytes(1024 * 1024))

        val provider = MetadataParser()
        val item = provider.parse(output)
        Assert.assertThat(item, Is.`is`(DownloadItem("Alcest - Wings", "wqY-Nl8mTyc")))
    }
}