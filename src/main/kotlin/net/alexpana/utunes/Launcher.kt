package net.alexpana.utunes

import net.alexpana.utunes.activity.download.ui.DownloaderWindow
import net.alexpana.utunes.activity.framesplit.ui.FrameSplitterWindow
import net.alexpana.utunes.ui.components.DarkFrame
import java.awt.FlowLayout
import javax.swing.JButton
import javax.swing.JPanel
import javax.swing.WindowConstants

class Launcher : DarkFrame() {

    private val downloaderWindow = DownloaderWindow()
    private val frameSplitterWindow = FrameSplitterWindow()

    init {
        title = "ManaBreak Tools v1.0.2"
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

        downloaderWindow.onClose {
            downloaderWindow.isVisible = false
        }

        frameSplitterWindow.onClose {
            frameSplitterWindow.isVisible = false
        }

        val contentPanel = JPanel()
        contentPanel.layout = FlowLayout()

        val downloaderButton = JButton("Youtube Downloader")
        downloaderButton.addActionListener {
            downloaderWindow.isVisible = true
        }

        val frameSplitterButton = JButton("Frame Splitter")
        frameSplitterButton.addActionListener {
            frameSplitterWindow.isVisible = true
        }

        contentPanel.add(frameSplitterButton)
        contentPanel.add(downloaderButton)

        pack()
        setSize(300, 70)
        setLocationRelativeTo(null)

        setContent(contentPanel)
    }
}