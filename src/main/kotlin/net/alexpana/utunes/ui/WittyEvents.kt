package net.alexpana.utunes.ui

object WittyEvents {
    private val events = arrayOf("marching poozers", "petting kittens", "spawning bosses", "destroying all musicals")
    private var index = 0

    fun nextEvent(): String {
        val result = events[index]
        index = (index + 1) % events.size

        return result
    }
}