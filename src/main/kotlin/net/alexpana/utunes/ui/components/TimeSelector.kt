package net.alexpana.utunes.ui.components

import java.awt.GridLayout
import javax.swing.JPanel
import javax.swing.JSpinner

class TimeSelector : JPanel() {
    private val hours: JSpinner = JSpinner()
    private val minutes: JSpinner = JSpinner()
    private val seconds: JSpinner = JSpinner()

    init {
        layout = GridLayout(1, 3)
        add(hours)
        add(minutes)
        add(seconds)
    }

    fun getTimePoint(): TimePoint {
        return TimePoint(hours.value as Int, minutes.value as Int, seconds.value as Int)
    }

    override fun setEnabled(enabled: Boolean) {
        hours.isEnabled = enabled
        minutes.isEnabled = enabled
        seconds.isEnabled = enabled
    }

    override fun isEnabled(): Boolean {
        return hours.isEnabled
    }
}