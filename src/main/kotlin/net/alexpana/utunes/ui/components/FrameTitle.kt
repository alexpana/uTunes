package net.alexpana.utunes.ui.components

import net.alexpana.utunes.ui.laf.DarkLaf
import java.awt.*
import java.awt.event.AWTEventListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.WindowEvent
import javax.swing.*

class FrameTitle : JPanel() {

    companion object {
        const val TITLE_HEIGHT = 24
    }

    private val buttonClose = CloseButton()
    private val buttonMinimize = MinimizeButton()
    private val titleLabel = JLabel()
    private val dragListener = AwtDragListener(this)

    private lateinit var _callback: (frame: DarkFrame) -> Unit

    var title: String
        get() = titleLabel.text
        set(value) {
            titleLabel.text = value
            repaint()
        }

    init {
        layout = CustomLayout()

        titleLabel.foreground = DarkLaf.foregroundColor
        titleLabel.border = BorderFactory.createEmptyBorder(0, 12, 0, 0)
        titleLabel.horizontalAlignment = SwingConstants.CENTER

        add(titleLabel)
        add(buttonMinimize)
        add(buttonClose)

        buttonMinimize.addActionListener {
            val frame = SwingUtilities.windowForComponent(this) as JFrame
            frame.state = Frame.ICONIFIED
        }

        buttonClose.addActionListener {
            val frame = SwingUtilities.windowForComponent(this) as JFrame
            frame.dispatchEvent(WindowEvent(frame, WindowEvent.WINDOW_CLOSING))
            frame.dispose()
        }

        preferredSize = Dimension(0, TITLE_HEIGHT)

        addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent?) {
                dragListener.beginDrag()
                Toolkit.getDefaultToolkit().addAWTEventListener(dragListener, AWTEvent.MOUSE_EVENT_MASK or AWTEvent.MOUSE_MOTION_EVENT_MASK)
            }
        })
    }

    fun onClose(callback: (frame: DarkFrame) -> Unit) {
        _callback = callback
    }

    private class CustomLayout : LayoutManager {
        override fun layoutContainer(parent: Container?) {
            val frameTitle = parent as FrameTitle

            frameTitle.titleLabel.bounds = Rectangle(0, 0, parent.width - parent.height * 2, parent.height)
            frameTitle.buttonMinimize.bounds = Rectangle(parent.width - parent.height * 2, 0, parent.height, parent.height)
            frameTitle.buttonClose.bounds = Rectangle(parent.width - parent.height, 0, parent.height, parent.height)
        }

        override fun preferredLayoutSize(parent: Container?): Dimension {
            return Dimension(parent?.width ?: 0, TITLE_HEIGHT)
        }

        override fun minimumLayoutSize(parent: Container?): Dimension {
            return Dimension(parent?.width ?: 0, TITLE_HEIGHT)
        }

        override fun addLayoutComponent(name: String?, comp: Component?) {
        }

        override fun removeLayoutComponent(comp: Component?) {
        }
    }

    class CloseButton : JButton() {
        init {
            border = BorderFactory.createEmptyBorder()
            cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
        }

        override fun paintComponent(g: Graphics?) {
            val padding = 8
            (g as Graphics2D).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

            if (model.isRollover) {
                g.color = Color(0xf04747)
            } else {
                g.color = DarkLaf.backgroundColor
            }

            g.fillRect(0, 0, width, height)

            g.color = Color(0x606060)
            if (model.isRollover) {
                g.color = Color(0xffffff)
            } else {
                g.color = Color(0x606060)
            }
            g.drawLine(padding, padding, width - padding - 1, height - padding - 1)
            g.drawLine(padding, height - padding - 1, width - padding - 1, padding)
        }
    }

    class MinimizeButton : JButton() {
        init {
            border = BorderFactory.createEmptyBorder()
            cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
        }

        override fun paintComponent(g: Graphics?) {
            val padding = 8

            (g as Graphics2D).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

            if (model.isRollover) {
                g.color = Color(0x282828)
            } else {
                g.color = DarkLaf.backgroundColor
            }

            g.fillRect(0, 0, width, height)

            g.color = Color(0x606060)
            g.drawLine(padding, height - padding - 1, width - padding - 1, height - padding - 1)
        }
    }

    class AwtDragListener(private val component: JComponent) : AWTEventListener {

        private var dragStart = Point(0, 0)
        private var frameStart = Point(0, 0)
        private var dragging = false

        override fun eventDispatched(event: AWTEvent?) {
            if (event?.id == MouseEvent.MOUSE_RELEASED) {
                Toolkit.getDefaultToolkit().removeAWTEventListener(this)
                dragging = false
            }

            if (event?.id == MouseEvent.MOUSE_DRAGGED && dragging) {
                val mousePos = MouseInfo.getPointerInfo().location
                val frame = SwingUtilities.getWindowAncestor(component)
                val point = Point(frameStart.x + mousePos.x - dragStart.x, frameStart.y + mousePos.y - dragStart.y)

                frame.location = point
            }
        }

        fun beginDrag() {
            val frame = SwingUtilities.getWindowAncestor(component)
            dragStart = MouseInfo.getPointerInfo().location
            frameStart = frame.bounds.location
            dragging = true
        }
    }
}