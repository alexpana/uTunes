package net.alexpana.utunes.ui.components

import net.alexpana.utunes.ui.laf.DarkLaf
import java.awt.*
import javax.swing.JComponent
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.SwingConstants

class BootstrapPanel : JPanel() {

    private val loadingLabel = JLabel("Loading")
    private val resourceLabel = JLabel("marching poozers")
    private var isError = false

    init {
        loadingLabel.font = DarkLaf.openSansBoldItalicFont?.deriveFont(30f)
        loadingLabel.horizontalAlignment = SwingConstants.CENTER
        loadingLabel.foreground = Color(0xeeeeee)

        resourceLabel.font = DarkLaf.openSansItalicFont?.deriveFont(18f)
        resourceLabel.horizontalAlignment = SwingConstants.CENTER
        resourceLabel.foreground = Color(0x555555)

        layout = Layout()
        add(loadingLabel)
        add(resourceLabel)
        background = DarkLaf.backgroundColor
    }

    fun setState(resource: String) {
        loadingLabel.text = "Loading"
        resourceLabel.text = resource
        resourceLabel.font = DarkLaf.openSansItalicFont?.deriveFont(18f)
        resourceLabel.foreground = Color(0x555555)
    }

    fun setError(error: String) {
        loadingLabel.text = "Oh noes!"
        resourceLabel.text = error
        resourceLabel.font = DarkLaf.openSansItalicFont?.deriveFont(12f)
        resourceLabel.foreground = Color(0xf75454)
    }

    fun setComponent(component: JComponent) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    class Layout : LayoutManager {
        private val loadingLabelHeight = 50
        private val resourceLabelHeight = 30

        override fun layoutContainer(parent: Container?) {
            (parent as BootstrapPanel)

            val verticalOffset = -40

            parent.loadingLabel.bounds = Rectangle(0, (parent.height - loadingLabelHeight - resourceLabelHeight) / 2 + verticalOffset, parent.width, loadingLabelHeight)
            parent.resourceLabel.bounds = Rectangle(0, (parent.height - loadingLabelHeight - resourceLabelHeight) / 2 + loadingLabelHeight + verticalOffset, parent.width, resourceLabelHeight)
        }

        override fun preferredLayoutSize(parent: Container?): Dimension {
            return Dimension(0, loadingLabelHeight + resourceLabelHeight)
        }

        override fun minimumLayoutSize(parent: Container?): Dimension {
            return Dimension(0, loadingLabelHeight + resourceLabelHeight)
        }

        override fun addLayoutComponent(name: String?, comp: Component?) {
        }

        override fun removeLayoutComponent(comp: Component?) {
        }

    }
}