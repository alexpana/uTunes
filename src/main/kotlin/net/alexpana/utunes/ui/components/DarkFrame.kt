package net.alexpana.utunes.ui.components

import net.alexpana.utunes.ui.laf.DarkLaf
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Container
import javax.swing.BorderFactory
import javax.swing.JFrame

open class DarkFrame : JFrame() {

    private val titlePanel = FrameTitle()

    private var _callback: ((frame: DarkFrame) -> Unit)? = null

    init {
        isUndecorated = true

        val rootPane = getRootPane()
        rootPane.background = DarkLaf.backgroundColor
        rootPane.border = BorderFactory.createLineBorder(Color(0x404040))
        rootPane.layout = BorderLayout()
        rootPane.add(titlePanel, BorderLayout.NORTH)

        titlePanel.onClose {
            if (_callback != null) {
                _callback?.invoke(this)
            } else {
                dispose()
            }
        }
    }

    fun setContent(content: Container) {
        rootPane.add(content, BorderLayout.CENTER)

    }

    override fun getTitle(): String {
        return titlePanel.title
    }

    override fun setTitle(value: String) {
        titlePanel.title = value
    }

    fun onClose(callback: (frame: DarkFrame) -> Unit) {
        _callback = callback
    }
}