package net.alexpana.utunes.ui.components

data class TimePoint(val hours: Int, val minutes: Int, val seconds: Int) {

    override fun toString(): String {
        return "%02d:%02d:%02d.000".format(hours, minutes, seconds)
    }

    companion object {
        fun fromSeconds(seconds: Int): TimePoint {
            val hours = Math.floorDiv(seconds, (60 * 60))
            val minutes = Math.floorDiv(seconds - hours * 60 * 60, 60)
            return TimePoint(
                    hours, minutes, seconds % 60
            )
        }
    }
}