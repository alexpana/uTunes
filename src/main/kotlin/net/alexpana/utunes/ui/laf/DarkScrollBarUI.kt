package net.alexpana.utunes.ui.laf

import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Rectangle
import javax.swing.JButton
import javax.swing.JComponent
import javax.swing.plaf.basic.BasicScrollBarUI

class DarkScrollBarUI : BasicScrollBarUI() {
    companion object {
        @JvmStatic
        fun createUI(c: JComponent): DarkScrollBarUI {
            return DarkScrollBarUI()
        }
    }

    override fun installDefaults() {
        super.installDefaults()
    }

    override fun installComponents() {
        super.installComponents()
        scrollbar.preferredSize = Dimension(8, 0)
    }

    override fun createIncreaseButton(orientation: Int): JButton {
        return createZeroButton()
    }

    override fun createDecreaseButton(orientation: Int): JButton {
        return createZeroButton()
    }

    private fun createZeroButton(): JButton {
        val button = JButton()
        button.maximumSize = Dimension(0, 0)
        button.minimumSize = Dimension(0, 0)
        button.preferredSize = Dimension(0, 0)
        return button
    }

    override fun paintTrack(g: Graphics?, c: JComponent?, trackBounds: Rectangle?) {
        g?.color = Color(0x323232)
        g?.fillRect(trackRect.x, trackRect.y, trackRect.width, trackRect.height)
    }

    override fun paintThumb(g: Graphics?, c: JComponent?, thumbBounds: Rectangle?) {
        g?.color = Color(0x2a2a2a)
        g?.fillRoundRect(thumbRect.x, thumbRect.y, thumbRect.width, thumbRect.height, 4, 4)

    }

    override fun paint(g: Graphics?, c: JComponent?) {
        super.paint(g, c)
        println("thumbBounds = ${thumbBounds}")
    }
}