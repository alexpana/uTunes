package net.alexpana.utunes.ui.laf

import java.awt.Color
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.plaf.basic.BasicPanelUI

class DarkPanelUI : BasicPanelUI() {
    companion object {
        @JvmStatic
        fun createUI(c: JComponent): DarkPanelUI {
            return DarkPanelUI()
        }
    }

    override fun installDefaults(p: JPanel?) {
        p?.background = DarkLaf.backgroundColor
    }
}