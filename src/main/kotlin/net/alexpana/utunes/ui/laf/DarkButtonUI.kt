package net.alexpana.utunes.ui.laf

import java.awt.*
import javax.swing.AbstractButton
import javax.swing.BorderFactory
import javax.swing.JComponent
import javax.swing.plaf.basic.BasicButtonUI

class DarkButtonUI : BasicButtonUI() {

    companion object {
        val font = DarkLaf.openSansFont?.deriveFont(11f)
        @JvmStatic
        fun createUI(c: JComponent): DarkButtonUI {
            return DarkButtonUI()
        }
    }

    override fun installDefaults(button: AbstractButton?) {
        button?.background = DarkLaf.backgroundColor
        button?.border = BorderFactory.createEmptyBorder(6, 12, 6, 12)
        button?.cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)
        button?.isRolloverEnabled = true
    }

    override fun paint(g: Graphics?, c: JComponent?) {
        (c as AbstractButton)
        (g as Graphics2D)

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        g.font = font

        g.color = Color(0x454545)
        if (c.model.isRollover || c.model.isArmed) {
            g.color = Color(0x555555)
        }
        g.fillRoundRect(0, 0, c.width, c.height, 6, 6)

        g.color = Color(0xdddddd)
        val stringBounds = g.fontMetrics.getStringBounds(c.text, g)
        g.drawString(c.text, (c.width - stringBounds.width).toFloat() / 2f, (c.height + stringBounds.height).toFloat() / 2f - 4)
    }
}