package net.alexpana.utunes.ui.laf

import java.awt.Color
import java.awt.Font
import javax.swing.UIManager


class DarkLaf {
    companion object {
        var openSansItalicFont: Font? = null
        var openSansBoldItalicFont: Font? = null
        var openSansFont: Font? = null
        val backgroundColor = Color(0x323232)
        val foregroundColor = Color(0x909090)

        fun initialize() {
            UIManager.put("PanelUI", DarkPanelUI::class.java.name)
            UIManager.put("ScrollBarUI", DarkScrollBarUI::class.java.name)
            UIManager.put("ButtonUI", DarkButtonUI::class.java.name)
            UIManager.put("LabelUI", DarkLabelUI::class.java.name)
            loadFonts()
        }

        private fun loadFonts() {
            openSansBoldItalicFont = loadFont("fonts/OpenSans-BoldItalic.ttf")
            openSansItalicFont = loadFont("fonts/OpenSans-Italic.ttf")
            openSansFont = loadFont("fonts/OpenSans-Regular.ttf")
        }

        private fun loadFont(font: String): Font {
            val fontResource = DarkLaf::class.java.classLoader.getResourceAsStream(font)
            return Font.createFont(Font.TRUETYPE_FONT, fontResource)
        }
    }
}