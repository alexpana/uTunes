package net.alexpana.utunes.ui.laf

import java.awt.Color
import javax.swing.JComponent
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.plaf.basic.BasicLabelUI
import javax.swing.plaf.basic.BasicPanelUI

class DarkLabelUI : BasicLabelUI() {
    companion object {
        @JvmStatic
        fun createUI(c: JComponent): DarkLabelUI {
            return DarkLabelUI()
        }
    }

    override fun installDefaults(p: JLabel?) {
        p?.background = DarkLaf.backgroundColor
        p?.foreground = DarkLaf.foregroundColor
    }
}