package net.alexpana.utunes.tools

import java.io.File

class EmbeddedBinaries {
    companion object {
        fun extractResource(file: String) {
            val tmpDir = System.getProperty("java.io.tmpdir")
            val resourceTmpFile = File(tmpDir + "/uTunes/" + file)
            File(resourceTmpFile.parent).mkdirs()

            if (!resourceTmpFile.exists()) {
                val byteInputStream = EmbeddedBinaries::class.java.classLoader.getResourceAsStream(file)
                print(byteInputStream)
                val outputStream = resourceTmpFile.outputStream()
                byteInputStream.copyTo(outputStream)
                outputStream.close()
                byteInputStream.close()
            }
        }

        fun extractedResource(resource: String): String {
            val tmpDir = System.getProperty("java.io.tmpdir")
            return tmpDir + "uTunes" + "/" + resource
        }
    }
}