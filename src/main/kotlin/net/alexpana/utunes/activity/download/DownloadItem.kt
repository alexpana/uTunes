package net.alexpana.utunes.activity.download

class DownloadItem(val title: String, val id: String) {
    override fun toString(): String {
        return "DownloadItem(title='$title', id='$id')"
    }

    fun getUrl(): String {
        return "https://www.youtube.com/watch?v=%s".format(id)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DownloadItem

        if (title != other.title) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + id.hashCode()
        return result
    }


}