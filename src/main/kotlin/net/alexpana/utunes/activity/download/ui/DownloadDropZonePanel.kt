package net.alexpana.utunes.activity.download.ui

import mu.KLogging
import net.alexpana.utunes.activity.download.MediaFormat
import net.alexpana.utunes.ui.laf.DarkLaf
import java.awt.*
import java.awt.datatransfer.DataFlavor
import java.awt.dnd.DropTargetDragEvent
import java.awt.dnd.DropTargetDropEvent
import java.awt.dnd.DropTargetEvent
import java.awt.dnd.DropTargetListener
import java.util.concurrent.Executors
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.SwingUtilities
import javax.swing.TransferHandler


class DownloadDropZonePanel(val downloaderWindow: DownloaderWindow) : JPanel() {

    companion object : KLogging()

    private val executor = Executors.newSingleThreadExecutor()

    @Volatile
    private var readingMetadata = false

    private var isDragging = false

    private var dragSide = DragSide.LEFT

    enum class DragSide {
        LEFT, RIGHT
    }

    init {
        preferredSize = Dimension(400, 86)

        font = DarkLaf.openSansBoldItalicFont?.deriveFont(24.0f)

        transferHandler = object : TransferHandler() {

            override fun canImport(comp: JComponent?, transferFlavors: Array<out DataFlavor>?): Boolean {
                return !readingMetadata
            }

            override fun importData(support: TransferSupport?): Boolean {
                val url = support?.transferable?.getTransferData(DataFlavor.stringFlavor).toString()
                executor.execute {
                    try {
                        SwingUtilities.invokeLater {
                            readingMetadata = true
                            repaint()
                        }
                        val items = downloaderWindow.requestMetadata(url)
                        logger.info("Importing {} items", items.size)
                        if (dragSide == DragSide.LEFT) {
                            downloaderWindow.requestDownload(items, MediaFormat.AUDIO)
                        } else {
                            downloaderWindow.requestDownload(items, MediaFormat.VIDEO)
                        }
                    } finally {
                        SwingUtilities.invokeLater {
                            readingMetadata = false
                            repaint()
                        }
                    }
                }

                return true
            }
        }

        dropTarget.addDropTargetListener(object : DropTargetListener {
            override fun dropActionChanged(dtde: DropTargetDragEvent?) {
            }

            override fun drop(dtde: DropTargetDropEvent?) {
                isDragging = false
                repaint()
            }

            override fun dragOver(dtde: DropTargetDragEvent?) {
                dragSide = if (dtde?.location?.x ?: 0 < this@DownloadDropZonePanel.size.width / 2) {
                    DragSide.LEFT
                } else {
                    DragSide.RIGHT
                }
                repaint()
            }

            override fun dragExit(dte: DropTargetEvent?) {
                isDragging = false
                repaint()
            }

            override fun dragEnter(dtde: DropTargetDragEvent?) {
                isDragging = true
                repaint()
            }
        })

    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        (g as Graphics2D).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

        g.color = background
        g.fillRect(0, 0, width, height)

        if (isDragging) {
            paintDropTarget(g)
        } else {
            paintPassive(g)
        }
    }

    private fun paintDropTarget(g: Graphics?) {
        val activeColor = Color(0x904040)
        val passiveColor = Color(0x505050)

        (g as Graphics2D)

        if (dragSide == DragSide.LEFT) {
            g.color = activeColor
        } else {
            g.color = passiveColor
        }
        g.fillRoundRect(0, 0, width / 2, height, 8, 8)
        g.color = Color(0xdddddd)
        paintText(g, "audio", - size.width / 4)

        if (dragSide == DragSide.RIGHT) {
            g.color = activeColor
        } else {
            g.color = passiveColor
        }
        g.fillRoundRect(width/2, 0, width / 2, height, 8, 8)
        g.color = Color(0xdddddd)
        paintText(g, "video", size.width / 4)

    }

    private fun paintPassive(g: Graphics?) {
        (g as Graphics2D)
        g.color = Color(0x404040)

        g.fillRoundRect(0, 0, width, height, 8, 8)

        g.font = font

        g.color = Color(0xdddddd)

        if (readingMetadata) {
            paintText(g, "Reading Metadata...")
        } else {
            paintText(g, "Drop URL Here")
        }

    }

    private fun paintText(g: Graphics2D, text: String, hOffset: Int = 0) {
        // Hardcoded because of lousy swing font metrics. Font size is 24.
        val halfAscent = 8

        val bounds = g.fontMetrics.getStringBounds(text, g)
        g.drawString(text, (width - bounds.width).toInt() / 2 + hOffset, (height / 2) + halfAscent)
    }
}