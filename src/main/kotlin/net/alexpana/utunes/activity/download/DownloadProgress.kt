package net.alexpana.utunes.activity.download

class DownloadProgress {
    enum class State(val uiValue: String) {
        NOT_STARTED("Pending"),
        PREPARING("Preparing"),
        DOWNLOADING("Downloading"),
        CONVERT_START("Converting"),
        CONVERT_END("Converting"),
        DONE("Done");

        override fun toString(): String {
            return uiValue
        }
    }

    var state: State = State.NOT_STARTED
    var progress: Float = 0f

    constructor(state: State, progress: Float) {
        this.state = state
        this.progress = progress
    }

    constructor(state: State) {
        this.state = state
    }

    override fun toString(): String {
        return "DownloadProgress(state=$state, progress=$progress)"
    }


}