package net.alexpana.utunes.activity.download

enum class MediaFormat {
    AUDIO,
    VIDEO
}