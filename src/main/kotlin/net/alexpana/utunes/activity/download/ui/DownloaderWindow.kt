package net.alexpana.utunes.activity.download.ui

import mu.KLogging
import net.alexpana.utunes.activity.download.*
import net.alexpana.utunes.activity.download.DownloadProgress.State.*
import net.alexpana.utunes.bootstrap.Bootstrap
import net.alexpana.utunes.bootstrap.BootstrapFactory
import net.alexpana.utunes.ui.WittyEvents
import net.alexpana.utunes.ui.components.BootstrapPanel
import net.alexpana.utunes.ui.components.DarkFrame
import net.alexpana.utunes.ui.laf.DarkLaf
import java.awt.*
import java.nio.file.FileSystems
import java.util.*
import java.util.concurrent.Executors
import javax.swing.*
import javax.swing.table.AbstractTableModel
import javax.swing.table.DefaultTableCellRenderer

class DownloaderWindow : DarkFrame() {
    companion object : KLogging()

    // Components
    private val executionTableModel = ExecutionTableModel()
    private val executionTable = JTable(executionTableModel)
    private val showFilesButton = JButton("Show Files")
    private val clearButton = JButton("Clear")

    // Panels
    private val contentPanel = JPanel()
    private val downloadPanel = JPanel()
    private val bootstrapPanel = BootstrapPanel()

    // State
    private val executor = Executors.newFixedThreadPool(5)
    private val bootstrap: Bootstrap

    init {
        title = "Youtube Downloader"
        size = Dimension(400, 800)

        bootstrap = BootstrapFactory.createBootstrap()

        initComponents()
        startBootstrap()

        pack()
        setLocationRelativeTo(null)
    }

    private fun initComponents() {
        val dropZonePanel = DownloadDropZonePanel(this)

        val cellRenderer = CellRenderer()
        executionTable.setShowGrid(false)
        executionTable.gridColor = DarkLaf.backgroundColor
        executionTable.background = DarkLaf.backgroundColor
        executionTable.tableHeader = null
        executionTable.columnModel.getColumn(0).preferredWidth = 600
        executionTable.columnModel.getColumn(0).cellRenderer = cellRenderer
        executionTable.columnModel.getColumn(1).preferredWidth = 140
        executionTable.columnModel.getColumn(1).cellRenderer = cellRenderer
        executionTable.border = BorderFactory.createEmptyBorder()

        val jScrollPane = JScrollPane(executionTable)
        jScrollPane.viewport.background = DarkLaf.backgroundColor
        jScrollPane.border = BorderFactory.createEmptyBorder()

        clearButton.addActionListener { executionTableModel.clear() }
        showFilesButton.addActionListener { Runtime.getRuntime().exec(bootstrap.externalResources().fileExplorer + " " + FileSystems.getDefault().getPath("./downloads")) }

        val actionPanel = JPanel()
        actionPanel.layout = FlowLayout(SwingConstants.RIGHT)
        actionPanel.add(clearButton)
        actionPanel.add(showFilesButton)

        downloadPanel.layout = BorderLayout(4, 4)
        downloadPanel.add(dropZonePanel, BorderLayout.NORTH)
        downloadPanel.add(jScrollPane, BorderLayout.CENTER)
        downloadPanel.add(actionPanel, BorderLayout.SOUTH)

        contentPanel.layout = BorderLayout()
        contentPanel.border = BorderFactory.createLineBorder(Color(0x323232), 4)

        setContent(contentPanel)
    }

    private fun startBootstrap() {
        setBootstrapView()
        bootstrapPanel.setState("n/a")

        bootstrap.bootstrap(object : Bootstrap.Listener {
            override fun onEvent(event: Bootstrap.Event) {
                processBootstrapEvent(event)
            }
        })
    }

    private fun processBootstrapEvent(event: Bootstrap.Event) {
        SwingUtilities.invokeLater {
            if (event.type == Bootstrap.Event.Type.ERROR) {
                bootstrapPanel.setError(event.message)
            }

            if (event.type == Bootstrap.Event.Type.LOADING) {
                bootstrapPanel.setState(WittyEvents.nextEvent())
                invalidate()
            }

            if (event.type == Bootstrap.Event.Type.DONE) {
                setDownloadView()
                pack()
            }

            if (event.type == Bootstrap.Event.Type.HELP) {
                if (event.component != null) {
                    bootstrapPanel.setComponent(event.component)
                }
                invalidate()
            }
        }
    }

    private fun setDownloadView() {
        contentPanel.removeAll()
        contentPanel.add(downloadPanel, BorderLayout.CENTER)
        invalidate()
    }

    private fun setBootstrapView() {
        contentPanel.removeAll()
        contentPanel.add(bootstrapPanel, BorderLayout.CENTER)
    }

    fun requestMetadata(url: String): List<DownloadItem> {
        return MetadataProvider(bootstrap.externalResources()).getMetadata(url)
    }

    fun requestDownload(items: List<DownloadItem>, mediaFormat: MediaFormat) {
        items.forEach {
            executionTableModel.updateProgress(it, DownloadProgress(NOT_STARTED))
            executor.execute {
                val downloader = ItemDownloader(bootstrap.externalResources())
                logger.info { "Downloading ${it.id}" }
                downloader.download(it, mediaFormat, object : ItemDownloader.Listener {
                    override fun progress(progress: DownloadProgress) {
                        executionTableModel.updateProgress(it, progress)
                    }
                })
            }
        }
    }

    @Suppress("unused")
    fun addDummyData() {
        executionTableModel.updateProgress(DownloadItem("Alcest - Wings", "0"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Opale", "1"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - La Nuit Marche Avec Moi", "2"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Voix Sereines", "3"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - L'Eveil Des Muses", "4"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Shelter", "5"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Away (feat. Neil Halstead / Slowdive)", "6"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Deliverance", "7"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Into the Waves", "8"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Wings", "10"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Opale", "11"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - La Nuit Marche Avec Moi", "12"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Voix Sereines", "13"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - L'Eveil Des Muses", "14"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Shelter", "15"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Away (feat. Neil Halstead / Slowdive)", "16"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Deliverance", "17"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Into the Waves", "18"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Wings", "20"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Opale", "21"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - La Nuit Marche Avec Moi", "22"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Voix Sereines", "23"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - L'Eveil Des Muses", "24"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Shelter", "25"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Away (feat. Neil Halstead / Slowdive)", "26"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Deliverance", "27"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Into the Waves", "28"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Wings", "210"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Opale", "211"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - La Nuit Marche Avec Moi", "122"), DownloadProgress(DONE))
        executionTableModel.updateProgress(DownloadItem("Alcest - Voix Sereines", "123"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - L'Eveil Des Muses", "124"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Shelter", "215"), DownloadProgress(DOWNLOADING))
        executionTableModel.updateProgress(DownloadItem("Alcest - Away (feat. Neil Halstead / Slowdive)", "126"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Deliverance", "127"), DownloadProgress(NOT_STARTED))
        executionTableModel.updateProgress(DownloadItem("Alcest - Into the Waves", "128"), DownloadProgress(NOT_STARTED))
    }

    private class CellRenderer : DefaultTableCellRenderer() {
        val renderFont = Font("Tahoma", Font.BOLD, 11)

        override fun getTableCellRendererComponent(table: JTable?, value: Any?, isSelected: Boolean, hasFocus: Boolean, row: Int, column: Int): Component {
            val item = (table?.model as ExecutionTableModel).getItemAt(row)

            item.downloadProgress

            val component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column)

            component.background = DarkLaf.backgroundColor
            if (row % 2 == 0) {
                component.background = Color(0x383838)
            }

            if (column == 0) {
                (component as JLabel).border = BorderFactory.createEmptyBorder(0, 10, 0, 0)
                component.horizontalAlignment = SwingConstants.LEFT
            } else {
                (component as JLabel).border = BorderFactory.createEmptyBorder(0, 0, 0, 10)
                component.horizontalAlignment = SwingConstants.RIGHT
            }

            component.font = renderFont
            component.foreground = Color(0xCD903C)
            if (item.downloadProgress.state == NOT_STARTED) {
                component.foreground = Color(0x707070)
            }
            if (item.downloadProgress.state == DONE) {
                component.foreground = Color(0x177D17)
            }

            return component
        }

    }

    private class DownloadItemProgress(val downloadItem: DownloadItem, val downloadProgress: DownloadProgress)

    private class ExecutionTableModel : AbstractTableModel() {

        private val rows = LinkedList<DownloadItemProgress>()

        override fun getRowCount(): Int {
            return rows.size
        }

        override fun getColumnCount(): Int {
            return 2
        }

        override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
            return when (columnIndex) {
                0 -> rows[rowIndex].downloadItem.title
                1 -> rows[rowIndex].downloadProgress.state
                2 -> if (rows[rowIndex].downloadProgress.progress == 0f) "" else rows[rowIndex].downloadProgress.progress.toInt()
                else -> "unknown"
            }
        }

        fun getItemAt(rowIndex: Int): DownloadItemProgress {
            return rows[rowIndex]
        }

        fun updateProgress(downloadItem: DownloadItem, downloadProgress: DownloadProgress) {
            val row = rows.find { it -> it.downloadItem == downloadItem }

            if (row == null) {
                rows.add(DownloadItemProgress(downloadItem, downloadProgress))
                fireTableDataChanged()
                return
            }

            row.downloadProgress.progress = downloadProgress.progress
            row.downloadProgress.state = downloadProgress.state

            fireTableDataChanged()
        }

        fun clear() {
            rows.removeIf { it.downloadProgress.state == DONE }
            fireTableDataChanged()
        }
    }
}