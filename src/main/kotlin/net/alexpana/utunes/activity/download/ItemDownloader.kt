package net.alexpana.utunes.activity.download

import mu.KLogging
import net.alexpana.utunes.activity.download.DownloadProgress.State.*
import net.alexpana.utunes.bootstrap.Bootstrap
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*
import java.util.regex.Pattern

class ItemDownloader(externalResources: Bootstrap.ExternalResources) {

    companion object : KLogging()

    private val downloaderPath = externalResources.youtubedl

    private val ffmpegPath = externalResources.ffmpeg

    private val outputFormat = "downloads/%(title)s.%(ext)s"

    fun download(item: DownloadItem, mediaFormat: MediaFormat, listener: Listener) {
        var state: DownloadProgress.State = NOT_STARTED

        val args = when (mediaFormat) {
            MediaFormat.AUDIO -> getAudioArgs(item)
            MediaFormat.VIDEO -> getVideoArgs(item)
        }

        logger.info { "Running: ${args.joinToString(" ")}" }

        val process = ProcessBuilder(args).start()
        val stdOutStream = BufferedReader(InputStreamReader(process.inputStream))
        val stdErrStream = BufferedReader(InputStreamReader(process.errorStream))

        var stdOutEnd = false
        var stdErrEnd = false

        while (!stdOutEnd || !stdErrEnd) {
            val outLine = stdOutStream.readLine()
            stdOutEnd = outLine == null

            if (outLine != null) {
                logger.info { "Output (stdout): $outLine" }

                if (outLine.startsWith("[youtube]") && state != PREPARING) {
                    state = PREPARING
                    listener.progress(DownloadProgress(state))
                }

                if (outLine.startsWith("[download]")) {
                    state = DOWNLOADING
                    val progress = if (outLine.contains("%")) outLine.split(Pattern.compile("\\s+"))[1].dropLast(1).toFloat() else 0f
                    listener.progress(DownloadProgress(state, progress))
                }

                if (outLine.startsWith("[ffmpeg]") && state != CONVERT_START) {
                    state = CONVERT_START
                    listener.progress(DownloadProgress(CONVERT_START, 0f))
                }

                if (outLine.startsWith("Deleting") && state != CONVERT_END) {
                    state = CONVERT_END
                    listener.progress(DownloadProgress(CONVERT_END))
                }
            }

            if (stdErrStream.ready() || !process.isAlive) {
                val errLine = stdErrStream.readLine()
                if (errLine != null) {
                    logger.info { "Output (stderr): $errLine" }
                }
                stdErrEnd = errLine == null
            }
        }

        listener.progress(DownloadProgress(DONE))
    }

    private fun getAudioArgs(item: DownloadItem) =
            Arrays.asList(downloaderPath, "-x", "--audio-format", "mp3", "--ffmpeg-location", ffmpegPath, "-o", outputFormat, item.getUrl())

    private fun getVideoArgs(item: DownloadItem) =
            Arrays.asList(downloaderPath, "-f", "bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best", "--ffmpeg-location", ffmpegPath, "-o", outputFormat, item.getUrl())

    interface Listener {
        fun progress(progress: DownloadProgress)
    }
}