package net.alexpana.utunes.activity.download

import mu.KLogging
import net.alexpana.utunes.bootstrap.Bootstrap
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*


class MetadataProvider(externalResources: Bootstrap.ExternalResources) {
    companion object : KLogging()

    private val downloaderPath = externalResources.youtubedl

    fun getMetadata(url: String): List<DownloadItem> {

        val args = Arrays.asList(downloaderPath, "-x", "--flat-playlist", "-j", url)

        logger.info { "Running: " + args.joinToString(" ") }

        val process = ProcessBuilder(args).start()
        val r = BufferedReader(InputStreamReader(process.inputStream))

        val lines = ArrayList<String>()

        var output = r.readLine()
        while (output != null) {
            logger.info { "Output: $output" }
            lines.add(output)
            output = r.readLine()
        }

        logger.info { "Waiting for youtube-dl process to finish" }

        process.waitFor()

        logger.info { "Process finished" }

        val parser = MetadataParser()

        return lines.map { parser.parse(it) }.toList()
    }
}