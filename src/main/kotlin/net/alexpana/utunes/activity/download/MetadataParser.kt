package net.alexpana.utunes.activity.download

import com.google.gson.JsonElement
import com.google.gson.JsonParser

class MetadataParser {
    fun parse(json: String): DownloadItem {
        val parser = JsonParser()
        val parse: JsonElement = parser.parse(json)

        val id = parse.asJsonObject.get("id").asString

        val title = if (parse.asJsonObject.has("title")) parse.asJsonObject.get("title").asString else "unknown"

        return DownloadItem(title, id)
    }
}