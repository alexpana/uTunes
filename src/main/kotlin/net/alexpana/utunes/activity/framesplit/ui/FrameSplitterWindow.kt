package net.alexpana.utunes.activity.framesplit.ui

import mu.KLogging
import net.alexpana.utunes.activity.framesplit.SplitProgress
import net.alexpana.utunes.activity.framesplit.SplitProgress.State.DONE
import net.alexpana.utunes.activity.framesplit.SplitProgress.State.PREPARING
import net.alexpana.utunes.activity.framesplit.VideoSplitter
import net.alexpana.utunes.bootstrap.Bootstrap
import net.alexpana.utunes.bootstrap.BootstrapFactory
import net.alexpana.utunes.ui.WittyEvents
import net.alexpana.utunes.ui.components.BootstrapPanel
import net.alexpana.utunes.ui.components.DarkFrame
import net.alexpana.utunes.ui.components.TimePoint
import net.alexpana.utunes.ui.components.TimeSelector
import java.awt.*
import java.io.File
import java.nio.file.FileSystems
import javax.swing.*

class FrameSplitterWindow : DarkFrame(), VideoSplitter.Listener {
    companion object : KLogging()

    // Components
    private val timeSelector = TimeSelector()
    private val duration = JSpinner()
    private val progressBar = JProgressBar()
    private val startButton = JButton("Start")
    private val showFilesButton = JButton("Show Files")

    // Panels
    private val contentPanel = JPanel()
    private val splitParametersPanel = JPanel()
    private val bootstrapPanel = BootstrapPanel()

    // State
    private val bootstrap: Bootstrap

    init {
        title = "Frame Splitter"
        size = Dimension(400, 250)

        bootstrap = BootstrapFactory.createBootstrap()

        initComponents()
        startBootstrap()

        pack()
        setLocationRelativeTo(null)
    }

    private fun initComponents() {
        val dropZonePanel = FrameSplitterDropZonePanel(this)

        showFilesButton.addActionListener { Runtime.getRuntime().exec(bootstrap.externalResources().fileExplorer + " " + FileSystems.getDefault().getPath("./frames")) }

        startButton.addActionListener { requestSplit(dropZonePanel.file!!, timeSelector.getTimePoint(), duration.value as Int) }

        progressBar.minimum = 0
        progressBar.maximum = 100

        val actionPanel = JPanel()
        actionPanel.layout = FlowLayout(SwingConstants.RIGHT)
        actionPanel.add(startButton)
        actionPanel.add(showFilesButton)

        val containerPanel = JPanel(BorderLayout())

        val parameterValuesPanel = JPanel()
        parameterValuesPanel.layout = GridLayout(2, 2)
        parameterValuesPanel.add(JLabel("From"))
        parameterValuesPanel.add(timeSelector)
        parameterValuesPanel.add(JLabel("Duration"))
        parameterValuesPanel.add(duration)

        containerPanel.add(parameterValuesPanel, BorderLayout.CENTER)
        containerPanel.add(progressBar, BorderLayout.SOUTH)

        splitParametersPanel.layout = BorderLayout(4, 4)
        splitParametersPanel.add(dropZonePanel, BorderLayout.NORTH)
        splitParametersPanel.add(containerPanel, BorderLayout.CENTER)
        splitParametersPanel.add(actionPanel, BorderLayout.SOUTH)

        contentPanel.layout = BorderLayout()
        contentPanel.border = BorderFactory.createLineBorder(Color(0x323232), 4)

        setContent(contentPanel)
    }

    private fun startBootstrap() {
        setBootstrapView()
        bootstrapPanel.setState("n/a")

        bootstrap.bootstrap(object : Bootstrap.Listener {
            override fun onEvent(event: Bootstrap.Event) {
                processBootstrapEvent(event)
            }
        })
    }

    private fun processBootstrapEvent(event: Bootstrap.Event) {
        SwingUtilities.invokeLater {
            if (event.type == Bootstrap.Event.Type.ERROR) {
                bootstrapPanel.setError(event.message)
            }

            if (event.type == Bootstrap.Event.Type.LOADING) {
                bootstrapPanel.setState(WittyEvents.nextEvent())
                invalidate()
            }

            if (event.type == Bootstrap.Event.Type.DONE) {
                setDownloadView()
                pack()
            }

            if (event.type == Bootstrap.Event.Type.HELP) {
                if (event.component != null) {
                    bootstrapPanel.setComponent(event.component)
                }
                invalidate()
            }
        }
    }

    private fun setDownloadView() {
        contentPanel.removeAll()
        contentPanel.add(splitParametersPanel, BorderLayout.CENTER)
        invalidate()
    }

    private fun setBootstrapView() {
        contentPanel.removeAll()
        contentPanel.add(bootstrapPanel, BorderLayout.CENTER)
    }

    private fun setUiEnabled(enabled: Boolean) {
        timeSelector.isEnabled = enabled
        duration.isEnabled = enabled
        startButton.isEnabled = enabled
    }

    override fun progress(progress: SplitProgress) {
        if (progress.state == PREPARING) {
            progressBar.isIndeterminate = true
        } else {
            progressBar.isIndeterminate = false
            progressBar.value = progress.progress.toInt()
        }
        setUiEnabled(progress.state == DONE)
    }

    fun requestSplit(file: File, start: TimePoint, duration: Int) {
        VideoSplitter(bootstrap.externalResources()).split(VideoSplitter.Arguments(file, start, duration), this)
    }

}