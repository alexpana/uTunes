package net.alexpana.utunes.activity.framesplit

import mu.KLogging
import net.alexpana.utunes.bootstrap.Bootstrap
import net.alexpana.utunes.ui.components.TimePoint
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.util.*
import java.util.concurrent.Executors

class VideoSplitter(externalResources: Bootstrap.ExternalResources) {

    companion object : KLogging()

    private val executor = Executors.newSingleThreadExecutor()

    private val ffmpegPath = externalResources.ffmpeg

    private val outputPath = "frames/"

    interface Listener {
        fun progress(progress: SplitProgress)
    }

    data class Arguments(val file: File, val time: TimePoint, val duration: Int)

    fun split(arguments: Arguments, listener: Listener) {
        executor.execute { doSplit(arguments, listener) }
    }

    private fun doSplit(arguments: Arguments, listener: Listener) {
        val state = SplitProgress.State.PREPARING

        val outputFolder = File(outputPath)
        if (!outputFolder.exists()) {
            outputFolder.mkdir()
        }

        val frameFormat = outputPath + "${arguments.file.nameWithoutExtension}_frame_%04d.jpg"

        val args = Arrays.asList(ffmpegPath + File.separator + "ffmpeg", "-i", arguments.file.absolutePath,
                "-ss", arguments.time.toString(),
                "-t", TimePoint.fromSeconds(arguments.duration).toString(),
                frameFormat)

        logger.info { "Running: ${args.joinToString(" ")}" }

        val process = ProcessBuilder(args).start()
        val stdOutStream = BufferedReader(InputStreamReader(process.inputStream))
        val stdErrStream = BufferedReader(InputStreamReader(process.errorStream))

        var stdOutEnd = false
        var stdErrEnd = false

        listener.progress(SplitProgress(state, 0f))

        while (!stdOutEnd || !stdErrEnd) {
            val outLine = stdOutStream.readLine()
            stdOutEnd = outLine == null

            if (stdErrStream.ready() || !process.isAlive) {
                val errLine = stdErrStream.readLine()
                if (errLine != null) {
                    logger.info { "Output (stderr): $errLine" }
                }
                stdErrEnd = errLine == null
            }
        }

        logger.info("Process exited with ${process.exitValue()}")
        listener.progress(SplitProgress(SplitProgress.State.DONE, 100.0f))
    }
}