package net.alexpana.utunes.activity.framesplit.ui

import mu.KLogging
import net.alexpana.utunes.ui.laf.DarkLaf
import java.awt.*
import java.awt.datatransfer.DataFlavor
import java.io.File
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.TransferHandler


class FrameSplitterDropZonePanel(val downloaderWindow: FrameSplitterWindow) : JPanel() {

    companion object : KLogging()

    public var file: File? = null

    init {
        preferredSize = Dimension(400, 86)

        font = DarkLaf.openSansBoldItalicFont?.deriveFont(24.0f)

        transferHandler = object : TransferHandler() {

            override fun canImport(comp: JComponent?, transferFlavors: Array<out DataFlavor>?): Boolean {
                return true
            }

            override fun importData(support: TransferSupport?): Boolean {
                if (support?.transferable?.isDataFlavorSupported(DataFlavor.javaFileListFlavor) != true) {
                    return false
                }

                val transferData = support.transferable?.getTransferData(DataFlavor.javaFileListFlavor)
                println(transferData)

                file = (transferData as List<File>)[0]
                repaint()
                return true
            }
        }
    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        (g as Graphics2D).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

        g.color = background
        g.fillRect(0, 0, width, height)

        (g as Graphics2D)
        g.color = Color(0x404040)
        g.fillRoundRect(0, 0, width, height, 8, 8)
        g.font = font
        g.color = Color(0xdddddd)

        println(file)

        if (file == null) {
            paintText(g, "Drop File Here")
        } else {
            var fileName = file?.name ?: ""
            if (fileName.length > 30) {
                fileName = fileName.substring(0, 26) + "..."
            }
            paintText(g, fileName)
        }
    }

    private fun paintText(g: Graphics2D, text: String, hOffset: Int = 0) {
        // Hardcoded because of lousy swing font metrics. Font size is 24.
        val halfAscent = 8

        val bounds = g.fontMetrics.getStringBounds(text, g)
        g.drawString(text, (width - bounds.width).toInt() / 2 + hOffset, (height / 2) + halfAscent)
    }
}