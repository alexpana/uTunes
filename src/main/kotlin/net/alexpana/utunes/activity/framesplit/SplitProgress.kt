package net.alexpana.utunes.activity.framesplit

class SplitProgress {
    enum class State(val uiValue: String) {
        NOT_STARTED("Pending"),
        PREPARING("Preparing"),
        CONVERT_START("Splitting"),
        DONE("Done");

        override fun toString(): String {
            return uiValue
        }
    }

    var state: State = State.NOT_STARTED
    var progress: Float = 0f

    constructor(state: State, progress: Float) {
        this.state = state
        this.progress = progress
    }

    constructor(state: State) {
        this.state = state
    }

    override fun toString(): String {
        return "SplitProgress(state=$state, progress=$progress)"
    }


}