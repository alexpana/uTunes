package net.alexpana.utunes

import net.alexpana.utunes.ui.laf.DarkLaf
import java.io.IOException
import java.util.logging.LogManager

fun main(args: Array<String>) {
    initializeLookAndFeel()
    initializeLogging()
    val launcher = Launcher()
    launcher.isVisible = true
}

private fun initializeLookAndFeel() {
    DarkLaf.initialize()
}

private fun initializeLogging() {
    // Used for class loader
    class Dummy

    try {
        val configFile = Dummy::class.java.classLoader.getResourceAsStream("simplelogger.properties")
        LogManager.getLogManager().readConfiguration(configFile)
    } catch (ex: IOException) {
        println("WARNING: Could not open configuration file")
        println("WARNING: Logging not configured (console output only)")
    }
}
