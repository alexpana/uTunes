package net.alexpana.utunes.bootstrap

class ErrorBootstrap : Bootstrap {
    override fun bootstrap(listener: Bootstrap.Listener) {
        listener.onEvent(Bootstrap.Event(Bootstrap.Event.Type.ERROR, "We don't support your operating system yet"))
    }
}