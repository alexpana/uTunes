package net.alexpana.utunes.bootstrap

import java.util.concurrent.Executors

class FunnyBootstrap : Bootstrap {
    private val executor = Executors.newSingleThreadExecutor()

    override fun bootstrap(listener: Bootstrap.Listener) {
        executor.execute {
            while (true) {
                listener.onEvent(Bootstrap.Event(Bootstrap.Event.Type.LOADING, "one potato two potato"))
                Thread.sleep(2000)
            }
        }
    }

}