package net.alexpana.utunes.bootstrap

import javax.swing.JComponent

/**
 * Used to check and setup the environment before starting the application.
 */
interface Bootstrap {

    fun bootstrap(listener: Listener)

    fun externalResources(): ExternalResources = ExternalResources("/usr/local/bin/", "youtube-dl", "open")

    class Event(val type: Type, val message: String, val component: JComponent? = null) {
        enum class Type {
            ERROR,
            LOADING,
            DONE,
            HELP
        }
    }

    class ExternalResources(val ffmpeg: String, val youtubedl: String, val fileExplorer: String)

    interface Listener {
        fun onEvent(event: Event)
    }
}