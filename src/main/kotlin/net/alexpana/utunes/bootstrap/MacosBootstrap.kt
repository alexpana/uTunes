package net.alexpana.utunes.bootstrap

import net.alexpana.utunes.bootstrap.Bootstrap.Event.Type.*
import java.io.File
import java.util.*

class MacosBootstrap : Bootstrap {

    private var resources: Bootstrap.ExternalResources = Bootstrap.ExternalResources("/usr/local/bin", "youtube-dl", "open")

    override fun bootstrap(listener: Bootstrap.Listener) {
        val ffmpegPath = findResource("ffmpeg")
        if (ffmpegPath.isEmpty()) {
            listener.onEvent(Bootstrap.Event(ERROR, "Please install ffmpeg"))
            return
        }

        val youtubedlPath = findResource("youtube-dl")
        if (youtubedlPath.isEmpty()) {
            listener.onEvent(Bootstrap.Event(ERROR, "Please install youtube-dl"))
            return
        }

        resources = Bootstrap.ExternalResources(File(ffmpegPath).parent, youtubedlPath, "open")

        listener.onEvent(Bootstrap.Event(DONE, ""))
    }

    override fun externalResources() = resources

    private fun findResource(name: String): String {
        val which = ProcessBuilder(Arrays.asList("which", name)).start()
        which.waitFor()

        return String(which.inputStream.readBytes(1024)).trim()
    }

}