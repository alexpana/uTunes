package net.alexpana.utunes.bootstrap

object BootstrapFactory {
    private val osName = System.getProperty("os.name").toLowerCase()

    fun createBootstrap(): Bootstrap {
        if (isWindows()) {
            return WindowsBootstrap()
        }

        if (isUnix()) {
            return LinuxBootstrap()
        }

        if (isMac()) {
            return MacosBootstrap()
        }

        return ErrorBootstrap()
    }

    private fun isWindows(): Boolean {
        return osName.indexOf("win") >= 0
    }

    private fun isMac(): Boolean {
        return osName.indexOf("mac") >= 0
    }

    private fun isUnix(): Boolean {
        return osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0
    }

    @Suppress("unused")
    fun isSolaris(): Boolean {
        return osName.indexOf("sunos") >= 0
    }
}