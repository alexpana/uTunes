package net.alexpana.utunes.bootstrap

class LinuxBootstrap : Bootstrap {
    override fun bootstrap(listener: Bootstrap.Listener) {
        listener.onEvent(Bootstrap.Event(Bootstrap.Event.Type.ERROR, "Linux is great, but we don't handle it yet"))
    }
}