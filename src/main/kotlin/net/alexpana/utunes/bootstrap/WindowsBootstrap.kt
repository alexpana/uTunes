package net.alexpana.utunes.bootstrap

import net.alexpana.utunes.tools.EmbeddedBinaries
import java.util.concurrent.Executors

class WindowsBootstrap : Bootstrap {
    private var executor = Executors.newSingleThreadExecutor()

    override fun bootstrap(listener: Bootstrap.Listener) {
        val resources = arrayOf("bin/win64/ffmpeg.exe", "bin/win64/ffplay.exe", "bin/win64/ffprobe.exe", "bin/win64/youtube-dl.exe")
        executor.execute {
            for (resource in resources) {
                listener.onEvent(Bootstrap.Event(Bootstrap.Event.Type.LOADING, "Extracting $resource"))
                EmbeddedBinaries.extractResource(resource)
            }
        }
        listener.onEvent(Bootstrap.Event(Bootstrap.Event.Type.DONE, ""))
    }

    override fun externalResources(): Bootstrap.ExternalResources {
        return Bootstrap.ExternalResources(
                EmbeddedBinaries.extractedResource("bin/win64/ffmpeg.exe"),
                EmbeddedBinaries.extractedResource("bin/win64/youtube-dl.exe"),
                "explorer"
        )
    }
}